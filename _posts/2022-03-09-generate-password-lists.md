---
layout: post
title: generating password lists
tags: pt
---
[with hashcat](https://infinitelogins.com/2020/11/16/using-hashcat-rules-to-create-custom-wordlists/):
```
hashcat --force list.txt -r /usr/share/hashcat/rules/best64.rule -r /usr/share/hashcat/rules/toggles5.rule -r /usr/share/hashcat/rules/append_atsign.rule -r /usr/share/hashcat/rules/append_exclamation.rule --stdout | sort -u > list-uniq.txt
```
