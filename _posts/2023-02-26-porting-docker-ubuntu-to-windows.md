---
layout: post
title: easily port dockerized nodejs app from ubuntu to windows
---

note: assumes you already have a functioning / built nodejs app (e.g. [this sauce](https://stackoverflow.com/questions/54292224/how-to-run-node-js-with-gulp-inside-docker-and-find-the-right-bashsrc))
  

[key sauce](https://stackoverflow.com/questions/23935141/how-to-copy-docker-images-from-one-host-to-another-without-using-a-repository)
```
1. inside ubuntu, make a docker image

sudo docker save -o <path for generated tar file> <image name>
sudo docker save -o ./pacman.tar pacman

2. copy the resulting "pacman.tar" to windows

3. on windows, make sure docker-desktop and wsl are installed

https://docs.docker.com/engine/install/ <<-- download windows msi and install
wsl --install
wsl --update

4. on windows, make sure docker is running (just double-click docker-desktop icon if you used typical install)

5. on windows, load the docker image

docker load -i <path to image tar file>
docker load -i .\pacman.tar

6. check:

docker images

7. now run your nodejs app (e.g. pacman, on port 8080)

docker run -it -p 127.0.0.1:8080:8080 --entrypoint bash pacman

8. inside docker, run nodejs app

npm run serve
```
