---
layout: post
title: "powershell reconnaissance"
tags: pt
---
shameless plugged from [SANS](https://www.sans.org/blog/pen-test-poster-white-board-powershell-built-in-port-scanner/)
  

PowerShell port scanner:
```
1..1024 | % {echo ((new-object Net.Sockets.TcpClient).Connect("10.0.0.100",$_)) "Port $_ is open!"} 2>$null
```
  

Test-Netconnection scan a range of IPs for a single port:
```
foreach ($ip in 1..20) {Test-NetConnection -Port 80 -InformationLevel "Detailed" 192.168.1.$ip}
```
  
  
PS IP range & port range scanner:
```
1..20 | % { $a = $_; 1..1024 | % {echo ((new-object Net.Sockets.TcpClient).Connect("10.0.0.$a",$_)) "Port $_ is open!"} 2>$null}
```
  
  
PS test egress filtering:
```
1..1024 | % {echo ((new-object Net.Sockets.TcpClient).Connect("allports.exposed",$_)) "Port $_ is open!" } 2>$null
```
[big ref](http://www.blackhillsinfosec.com/?p=4811)