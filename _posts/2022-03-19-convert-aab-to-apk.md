---
layout: post
title: converting aab to apk (including signing apks)
tags: pt
---
first, make sure you have a [keystore](https://stackoverflow.com/questions/3997748/how-can-i-create-a-keystore) (direct way - use java's native keytool tool)
```
PS C:\Program Files\Java\jdk-11.0.14\bin> .\keytool -genkey -v -keystore c:\windows\tasks\my-release-key.keystore -alias alias_name -keyalg RSA -keysize 2048 -validity 10000
```
then use [bundletool](https://github.com/google/bundletool/releases) to convert aab to apks (yes, apks with an 's' first)
```
java -jar .\bundletool-all-1.9.0.jar build-apks --bundle=/MyApp/my_app.aab --output=/MyApp/my_app.apks --mode=universal
```
[then](https://stackoverflow.com/questions/53040047/generate-apk-file-from-aab-file-android-app-bundle):
```
MAIN STEP: Change the output file name from .apks to .zip
Unzip and explore
The file universal.apk is your app 
```
