---
layout: post
title: how javascript treats hyphens (xss tips)
tags: pt
---
[sauce](https://stackoverflow.com/questions/60593034/how-does-javascript-treat-hyphens)

The code that the site uses is:
```js
let jeff = (new URL(location).searchParams.get('jeff') || "JEFFF")
eval(`ma = "Ma name ${jeff}"`)
```
Note that searchParams gives you a URLSearchParams object, and its .get method gives you a string corresponding to the parameter. So, the objective is to come up with some characters that, when inserted into
```js
ma = "Ma name <CHARACTERS>"
```
and run, results in arbitrary code execution.  
First step is to surround the characters in "s, so as to end the string literal after the name and resume a string literal after the CHARACTERS:
```js
ma = "Ma name " <SOMETHING ELSE> ""
```
So now you need to figure out what sort of characters can go into <SOMETHING ELSE> which will result in valid Javascript code.  
If you just put in alert(), that won't be valid:
```js
ma = "Ma name " alert() ""
```
That's a syntax error. You need something to indicate what the alert has to do with the string literal token that comes just before it. A - can do the trick, but so could any other operator, like +, %, and so on. You also need to connect the end of the alert with the resumed string literal, thus the need for another operator at the end:
```js
ma = "Ma name "-alert('foo')-""
```
So, the characters that need to be inserted are:
```js
"-alert('foo')-"
```
Note that because the string is delivered inside of a search parameter, a + won't be interpreted as the literal character + - rather, it'll be interpreted as a space. So jeff="+alert(1337)+" won't work, but jeff="%2balert(1337)%2b" will.  
Semicolons work as well, because they result in:
```js
ma = "Ma name " <SOMETHING ELSE> ""
ma = "Ma name "; alert()        ;""
```
which is valid syntax.
