---
layout: post
title: easily make python virtual environment (venv)
tags: pt
---
[sauce](https://linuxconfig.org/how-to-set-up-a-python-virtual-environment-on-debian-10-buster)

1. make it 
```
python3 -m venv /path/to/virtual/environment
python3 -m venv ~/virtual
```
2. use it
```
source ~/virtual/bin/activate
```
3. when done
```
deactivate
```
python3.8 version
```
sudo apt update
sudo apt install software-properties-common
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt install python3.8
sudo apt install python3.8-venv
python3.8 -m venv /opt/venv-3.8
source /opt/venv-3.8/bin/activate
```
