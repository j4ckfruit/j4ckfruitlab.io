---
layout: post
title: amazing frida ssl-bypass guide, not my own
tags: pt mpt
---

### Some resources referred
[Defeating Android Certificate Pinning with Frida](https://httptoolkit.tech/blog/frida-certificate-pinning/)  
[Android Pen-testing - Bypass SSL pinning](https://www.youtube.com/watch?v=WXStzGTosSA)

### Setting up adb
1. Download adb [here](https://www.xda-developers.com/install-adb-windows-macos-linux/).
2. Extract the files. 
3. Run adb.exe from the extracted location.
4. To test if ADB is working, run:

```
# make sure the virtual Android device is running, or a physical Android device with USB debugging mode switched on is connected to the attacking machine

.\adb devices
```

### Setting up Frida framework

```
# make sure python3 is installed

pip install frida
pip install frida-tools
```

### Troubleshooting if "frida" command is not found
1. You may encounter situation where "frida" or "frida-ps" command is not found.
2. This is due to environment setting not configured properly.
3. Look for frida.exe in your machine (possibly located at "C:\Users\Username\AppData\Local\Packages\PythonSoftwareFoundation.Python.3.9_qbz5n2kfra8p0\LocalCache\local-packages\Python39\Scripts\")
4. Add to PATH 

### Getting Frida server
1. You may find all the releases [here](https://github.com/frida/frida/releases).
2. Look for the architecture that matches with the testing Android device.
3. Check the architecture with this command:

```
.\adb.exe shell getprop ro.product.cpu.abi
```

### Getting Frida SSL Unpinning .js script

[You can find the script here](https://raw.githubusercontent.com/httptoolkit/frida-android-unpinning/main/frida-script.js).

### Running the script

```
# copy the server file and the .js script to the Android device

.\adb.exe push frida-server-15.1.14-android-x86 /data/local/tmp
.\adb.exe push unpin2.js /data/local/tmp
```

```
# open a shell on the Android device
.\adb.exe shell

# switch to root user (optional)
su

# go to /data/local/tmp directory, with the server file is copied to
cd /data/local/tmp

# change the server file permission
chmod 755 fri*

# run the server file in the background
./frida-server-15.1.14-android-x86 &
```

```
# look for the app id for which you need to bypass certificate pinning

frida-ps -U | findstr -i yourapp
```

```
# run the script

frida -U 'YourApp' -l unpin2.js --no-pause
```

- Try to access the app function again for the bypass to take effect.

### Congrats, the bypass should work at this stage!!! :)
