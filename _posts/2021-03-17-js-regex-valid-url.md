---
layout: post
title: "javascript regex for validating URLs"
tags: pt javascript
---
probably the king of javascript regexes for validating URLs (revived thanks to [stackoverflow](https://stackoverflow.com/questions/5717093/check-if-a-javascript-string-is-a-url)):
```
function validURL(str) {
  var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
    '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
  return !!pattern.test(str);
}
```
saving for future generations.
