---
layout: post
title: "cert-based ntlm authentication with burp and openssl"
tags: pt openssl
---

so this is for when you absolutely have to interact with a website using a certain private key (.pem) and a certificate (.cer) file.
  

first, check that you can interact with the site with your private key (.pem) and certificate (.cer) file:
```
openssl s_client -connect <target ip>:<port> -key key.pem -cert <cert.cer>
```
if that worked, you're almost there. next, generate a PKCS12 certificate using openssl:
```
openssl pkcs12 -export -inkey key.pem -in <cert.cer> -out <cert.p12>
```
when prompted for a password, enter any random password, e.g. "12345"
- note here that your PKCS12 cert is `cert.p12`
- also note your passphrase for the PKCS12 cert - you'll need both for the next step
  
  
go to your burpsuite > project options > TLS > Client TLS Certificates, tick 'override user options', and click 'Add'
- add your Destination Host (e.g. target.site)
- check File (PKCS#12), click next
- click Select file, upload your PKCS12 certificate
- enter your random password from earlier
- finish the process
  
  
you should now be able to access the site with your PKCS12 cert :)
