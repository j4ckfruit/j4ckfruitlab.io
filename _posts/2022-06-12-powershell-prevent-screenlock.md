---
layout: post
title: powershell prevent screenlock/screensaver
tags: pt
---
[sauce](https://dmitrysotnikov.wordpress.com/2009/06/29/prevent-desktop-lock-or-screensaver-with-powershell/)

```
param($minutes = 60) #change no. of minutes accordingly

$myshell = New-Object -com "Wscript.Shell"

for ($i = 0; $i -lt $minutes; $i++) {
  Start-Sleep -Seconds 60
  $myshell.sendkeys(".")
}
```
how to use:  
1- open a powershell window  
2- copy/paste the above into powershell window (note: change no. of minutes accordingly)  
3- open a notepad to capture the no. of "." that the above script will send.  
4- to kill it, hit ctrl+c in the powershell window. to re-start it, just press "up" key in your powershell and hit enter.  
5- you're welcome :)
