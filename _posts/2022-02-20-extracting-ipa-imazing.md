---
layout: post
title: easily (kind of) extract ipa from iphone using imazing + some tricks
tags: pt
---
first use imazing and do these steps:
```
1. load your phone to imazing
2. click "Apps"
3. right-click the app you want, choose "Extract App"
```
you should now see your app stored on your disk with a .imazingapp extension.  
now comes the fun part:  
1. first, unzip the imazingapp thingy:
```
unzip app.imazingapp
```
2. see that 3 new things appear: Payload folder, Container folder, iTunesMetadata.plist. see that inside the Payload folder is a file with a .app extension named with your app - the .app thingy is just a directory. now copy everything _inside_ the Container folder into this .app folder:
```
cd Container
cp -r * ../Payload/yourapp.app/
```
3. also copy the iTunesMetadata.plist file into yourapp.app as Info.plist:
```
cd ..
cp iTunesMetadata.plist Payload/yourapp.app/Info.plist
```
4. now turn your Payload folder into an ipa by zipping it up and renaming it as ipa:
```
zip -r youripa.ipa Payload
```
now mobsf should recognize the ipa - we're good to go for static analysis!
