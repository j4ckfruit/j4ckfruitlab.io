---
layout: post
title: useful bash trix
tags: pt
---
bash bulk change prefix [sauce](https://unix.stackexchange.com/questions/47367/bulk-rename-change-prefix)
```
for f in TestSR*; do mv "$f" "CL${f#TestSR}"; done
```
bash delete empty files in current dir [sauce](https://www.baeldung.com/linux/delete-empty-files-dirs)
```
find . -type f -empty -print -delete
```
