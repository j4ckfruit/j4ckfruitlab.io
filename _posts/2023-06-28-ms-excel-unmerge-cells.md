---
layout: post
title: easily unmerge cells in ms excel
tags: pt
---
frustrated when merged cells screw up the functionality of a spreadsheet? worry no more
```
1. highlight the cell(s) you want to unmerge. feel free to select all that you want
2. home > down button at "merge and center" > unmerge cells
3. now your cells should be unmerged, but with empty rows below top row. keep the cells selected.
4. home > find & select > go to special
5. inside "go to special", select blanks. click ok
6. now all the blanks are selected
7. type: = + up button
8. now all the blank cells are pointing to one row up
9. press: ctrl + enter
10. voila
```
