---
title: optimizing mac screenshots
layout: post
tags: pt
---
### [setting screenshots location](https://www.hellotech.com/guide/for/how-to-change-where-screenshots-are-saved-on-mac):
1. open terminal
2. create a folder for your new screenshots location
```
mkdir /users/myusername/Desktop/Screenshots
```
3. set new location
```
defaults write com.apple.screencapture location /users/myusername/Desktop/Screenshots
```
4. reset mac UI
```
killall SystemUIServer
```

### [changing timestamp format to 24h (DD:MM:YYYY HH:MM:SS)](https://apple.stackexchange.com/questions/245771/change-date-time-format-in-the-screen-shot-filename)
```
1. System Preferences
2. Language & Region
3. click Advanced...
4. choose the Times tab 
5. for the Medium format click the hour dropdown and switch it from 1-12 to 00-23 and delete the AM/PM element
```
