---
layout: post
title: python tips and trix for hcr
tags: pt
---

```python
def observations():
	with open('obs.txt') as f:
		with open('rec.txt') as f2:
			line = f.readline().rstrip()
			line2 = f2.readline().rstrip()
			while line != "":
				print("We observed that the affected setting was to a value of \"%s\", instead of the recommended setting of \"%s\"." % (line,line2))
				line = f.readline().rstrip()
				line2 = f2.readline().rstrip()

def recommendations():
	'''
	We recommend to configure as per the benchmark setting, which is to 'Ensure 'Enforce password history' is set to '24 or more password(s)'
	'''
	with open('rec.txt') as f:
		with open('title.txt') as f2:
			line = f.readline().rstrip()
			line2 = f2.readline().rstrip()
			while line != "":
				print("We recommend to configure as per the benchmark setting, which is to configure \"%s\" to the recommended value of \"%s\"." % (line2,line))
				line = f.readline().rstrip()
				line2 = f2.readline().rstrip()

recommendations()
```
