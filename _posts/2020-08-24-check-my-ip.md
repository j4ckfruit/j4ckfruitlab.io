---
layout: post
title: "curling ipchicken to check your IP"
tags: pt
---
```
curl -s https://ipchicken.com | grep -E "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b"
```