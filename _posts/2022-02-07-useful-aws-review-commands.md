---
layout: post
title: useful aws review commands
tags: pt
---

note: in powershell context  

#### cloudformation 
```
gc .\stacks.txt | foreach-object { aws cloudformation describe-stacks --stack-name $_ }
```
```
aws cloudformation describe-stacks --stack-name u-internet-cloud-admin
```

#### cloudtrail
```
aws cloudtrail describe-trails
```
```
aws cloudtrail get-event-selectors --trail-name <trailname>
```

#### ebs 
```
aws ebs list-snapshot-blocks --snapshot-id snap-xxx
```

#### dynamodb
```
aws dynamodb describe-table --table-name dbs-dynamodb-xxx
```
```
aws dynamodb describe-continuous-backups --table-name dbs-dynamodb-xxx
```

#### ec2 [(security groups awesumsauce)](https://github.com/davidclin/aws-cli-get-security-groups)
```
gc .\ec2.txt | foreach-object { echo $_;aws ec2 describe-instances --instance-ids $_ --query 'Reservations[*].Instances[*].PrivateIpAddress' --output text }
```
```
aws ec2 describe-addresses
```
```
aws ec2 describe-volumes --volume-ids vol-xxx
```
```
gc .\ec2.txt | foreach-object { aws ec2 describe-instance-attribute --attribute userData --instance-id $_ }
```
```
aws ec2 describe-security-groups --query 'SecurityGroups[*].GroupId' --output text | % {$_ -replace "\t", "`n"}
```
```
aws ec2 describe-security-groups --group-ids <sgid>
```
```
aws ec2 describe-network-acls --query 'NetworkAcls[*].Associations[*].NetworkAclId'
```

#### ecs 
```
gc .\ecs.txt | foreach-object { aws ecs describe-task-definition --region ap-southeast-1 --task-definition $_ }
```

#### efs 
```
aws efs describe-file-system-policy --file-system-id fs-xxxxxx
```

#### elb
```
aws elb describe-load-balancers
aws elb describe-load-balancer-attributes --load-balancer-name <value>
```

#### elbv2 
```
aws elbv2 describe-load-balancers
aws elbv2 describe-load-balancer-attributes --load-balancer-arn <value>
```

#### iam 
```
aws iam get-policy-versions --policy-arn arn:aws:iam:xxxxxxx:policy/xxxxxx 
```
```
aws iam get-policy-version --policy-arn arn:aws:iam:xxxxxxx:policy/xxxxxx --version-id xx
```
```
aws iam list-mfa-devices
aws iam list-virtual-mfa-devices
```
```
aws iam get-account-password-policy
```
```
aws iam get-user
aws iam list-user-policies --user-name <username>
aws iam list-attached-user-policies --user-name <username>
```
```
aws iam get-role --role-name "rolename"
aws iam list-role-policies --role-name "rolename"
aws iam get-role-policy --role-name "rolename" --policy-name "policyname"
```
```
aws iam list-groups-for-user --user-name <username>
aws iam list-group-policies --group-name <groupname>
aws iam list-attached-group-policies --group-name <groupname>
aws iam get-policy --policy-arn <policyarn>
```

#### s3api
```
aws s3api list-object-versions --bucket <bucket-id>
```
```
aws s3api get-bucket-versioning --bucket <bucket-id>
```
```
gc .\s3.txt | foreach-object { aws s3api get-bucket-versioning --bucket $_ }
```
```
aws s3api get-bucket-logging --bucket <bucket-id>
```
```
aws s3api get-bucket-policy --bucket <bucket-id> --query Policy
```

#### sns
```
aws sns list-topics
```
```
aws sns get-topic-attributes --topic-arn <sns-arn>
```

#### sqs
```
aws sqs list-queues
```
```
aws sqs get-queue-attributes --attribute-names All --queue-url "<queueurl>"
```

#### ssm 
```
aws ssm describe-document --name CloudWatchAgentInstallAndConfigure
```
```
aws ssm get-document --name SSM-SessionManagerRunShell
```

#### sts 
```
aws sts get-caller-identity
```
