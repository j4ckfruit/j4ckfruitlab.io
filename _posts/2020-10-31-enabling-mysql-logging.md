---
layout: post
title: "enabling mysql logging"
tags: pt
---

go to `/etc/my.cnf` and append `log = <desired log file>` under the `[mysqld]` heading:
```
[mysqld]
(.. some other configs)
log = /var/log/mysql.log
```

notes: make sure your desired log exists and change ownership to mysql: e.g. `chown mysql:mysql /var/log/mysql.log`