---
layout: post
title: "Useful nmap cmds for big-sized nvas"
tags: nmap
---

pre-req: assuming we have ip lists in a folder called `lists` in a parent directory.

### sweep
do this in a folder called `nmap_sweep`
```
for i in $(ls ../lists/); do nmap -v -iL ../lists/$i -sn -oN nmap-sweep_$i;done
```

### top 1000
do this in a folder called `nmap_1000`
```
for i in $(ls ../lists/);do nmap -v -sC -sV -iL ../lists/$i -oN nmap-1000_$i;done
```
if you already have list of alive hosts (e.g. up_hosts_<subnet>)
```
for i in $(ls ../nmap_sweep/ | grep up_hosts | grep -v count); do nmap -v -sC -sV -iL ../nmap_sweep/$i -oN nmap-1000_$i;done
```

### getting alive hosts
do this inside the folder of nmap_sweep results
```
for i in $(ls | grep nmap);do echo "[+] $i" && cat $i | grep "report" | grep -v "down"|cut -d" " -f5;done
```

### a sweet sweep_script.sh for sizing up subnets
```
for i in $(ls ../lists/);do nmap -v -iL ../lists/$i -sn -oN nmap-sweep_$i.txt;done
for i in $(ls | grep nmap);do echo "[+] $i" && cat $i | grep "report" | grep -v "down"|cut -d" " -f5 >> up_hosts_$i.txt;done
for i in $(ls | grep up_hosts);do wc -l $i >> up_hosts_count.txt;done
```

### sort.sh for ensuring only IP addresses collected from nmap_sweep
```
for i in $(cat test);do
        if [[ $i =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]];then
                echo $i | cut -d" " -f4 >> output.txt
        fi
        if [[ $i =~ ^\([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\)$ ]];then
                echo $i | cut -d"(" -f2 | cut -d")" -f1 >> output.txt
        fi
        #echo $i;
done
```

### combining sweep_script.sh and sort.sh
```
echo "[*] performing nmap sweep on all lists in ../lists/ ..."
for i in $(ls ../lists/); do echo "[*] sweeping $i" && nmap -v -iL ../lists/$i -sn -oN nmap-sweep_$i.txt;done

for i in $(ls | grep nmap);do echo "[+] reading $i for up hosts ..." && cat $i | grep "report" | grep -v "down"|cut -d" " -f5 >> up_hosts_$i.txt;done

for i in $(ls | grep up_hosts);do
        j=`wc -l $i | cut -d" " -f1`
        if (( $j > 1000 )) ;then
                echo "[*] splitting $i into A and B due to line count > 1000 ..."
                round_down=$(($j/2))
                round_up=$((($j+1)/2))
                head -n$round_down $i >> ${i:0:-4}_A.txt
                tail -n$round_up $i >> ${i:0:-4}_B.txt
                rm $i
        fi
done
for i in $(ls | grep up_hosts);do wc -l $i >> up_hosts_count.txt;done
echo "[*] summary at up_hosts_count.txt"
```

### power sweep
```
-n -sn -PE -PP -PS21,22,23,25,80,113,443,31339 -PA80,113,443,10042
```

### under dev: caller-quick.py 
```
'''
setting up
'''
print("***********************************************************************************************")
print("** Scanning all up_hosts_* files with default options [1000ports / TCP / testssl with color] **")
print("***********************************************************************************************")
print("\n[*]collecting up_hosts_* files ...")
lists = os.popen("ls | grep up_hosts | grep -v count").read() #ls .. if running from inside 'nothing'
print (lists)
input("continue?")
#wait = 0
nmap_1000ports = True
nmap_option = "tcp"
want_colour = True

lists = lists.split('\n')[:-1] 

```