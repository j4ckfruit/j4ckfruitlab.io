---
layout: post
title: powershell for source code reviews (and hcr too)
tags: pt
---
```
gci -recurse | select-string "string i want"
gci -recurse | select-string "string i want" | out-string -stream | select-string "anotherstring"
gci -recurse | select-string "string i want" | out-string -stream | select-string -notmatch "notthis|notthistoo"
gci -recurse | select-string "string i want" -list | select -expandproperty path
gci -recurse | select-string "string i want" -context 1,1
```
```
gci -recurse "*filename*" | foreach-object { gci $_ | select-string "string i want" }
gci -recurse "*filename*" | foreach-object { gci $_ | select-string -pattern "^string i want starts with string" }
gci -recurse "*filename*" | foreach-object { gci $_ | select-string -pattern "^string i want starts with string" } | select-string "keyword"
```
