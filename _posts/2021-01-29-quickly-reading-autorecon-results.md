---
layout: post
title: "quickly reading autorecon results"
tags: pt
---
starting from `autorecon/results` folder:
  

listing results:
```
for i in $(ls);do ls $i/scans;done
```

reading quick nmap scans:
```
for i in $(ls);do cat $i/scans/_quick_tcp_nmap.txt;done
```
