---
layout: post
title: fixing common windows misconfigurations (nessus)
tags: pt
---
[insecure windows service permissions](https://www.tenable.com/plugins/nessus/65057)
```
Path : d:\program files\some\program\bin\executable.exe
Used by services : some_service
File write allowed for groups : Authenticated Users
```
```
icacls "d:\program files\some\program\bin\executable.exe"
icacls "d:\program files\some\program\bin\executable.exe" /remove:g "Authenticated Users"
icacls "d:\program files\some\program\bin\executable.exe" /grant:r "Authenticated Users":(RX)
icacls "d:\program files\some\program\bin\executable.exe"
```
[unquoted service paths](https://www.tenable.com/plugins/nessus/63155)
```
Nessus found the following service with an untrusted path : 
  some_service : C:\Windows\Some\Service\Executable\With Space In Path\Executable.exe
```
```
sc qc some_service
sc config some_service binpath= """C:\Windows\Some\Service\Executable\With Space In Path\Executable.exe"""
sc qc some_service
```
```
# manual check for all services with unquoted binpaths
wmic service get name,pathname,displayname,startmode | findstr /i auto | findstr /i /v "C:\Windows\\" | findstr /i /v """
```
