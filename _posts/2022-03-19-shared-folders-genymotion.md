---
layout: post
title: making shared folders for genymotion
tags: pt
---
[sauce](https://stackoverflow.com/questions/22472963/add-folder-files-in-genymotion-emulator)
```
1. Go to your VirtualBox VM setting / Shared folder tab
2. Add a shared folder with the folder you want to shared, and check the "auto mount" option
3. Start your VM as usual from the Genymotion software
4. Your shared folder is available in the /mnt/shared directory (multiple shared folders are supported)
```
(optional)
```
After that Change Settings of file Manager in Genymotion emulator change that to root user so all my folder visible also with Shared folders
- In Genymotion virtual device, run the application "File Manager" and READ the advertisement about "low-privileged mode".
- Go to "settings" menu from "File Manager"
- In "general settings", click on "access mode" and activate "Root access" mode
- Restart File Manager => all directories will be presented
```
