---
layout: post
title: useful regexes for everyday life
tag: pt
---
[first, this awesome testbed](https://regex101.com/)  
  
1. any string up till a ')' [source](https://stackoverflow.com/questions/6109882/regex-match-all-characters-between-two-strings)
```
(.*)(?=\))
```
```
18.9.99.2.1 (L1)
```
2. any string with "" and a +
```
^.*?query.*?(?i)".*?"\s\+\s
```
```
query = "SELECT asadfasdf something" + somevar + "something"
```
