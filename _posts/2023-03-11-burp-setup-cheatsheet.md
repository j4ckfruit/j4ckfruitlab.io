---
layout: post
title: burp setup cheatsheet
tags: pt
---

keep all subdomains within a primary domain in scope (e.g. *.web-security-academy.net)
```
# target > scope settings > check "advanced scope control" > add:
# key regex: .*.web-security-academy.net

protocol: https (or anything)
host or ip range: .*.web-security-academy.net (note: this is a regex)
port: <leave empty>
file: <leave empty>
```

load jython [sauce](https://portswigger.net/burp/documentation/desktop/extensions/installing-extensions)
```
1. download jython standalone -> https://www.jython.org/download.html
2. go to Extensions > Extension Settings > Python environment > select location of Jython standalone jar file
```

useful extensions
```
ActiveScan++
Autowasp
Content Type Converter
Copy As Python-Requests
Hackverter
ViewState Editor
```
