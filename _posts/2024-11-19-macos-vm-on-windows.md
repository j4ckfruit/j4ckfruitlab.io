---
title: easily install macos 14 on windows (vmware)
layout: post
tags: pt
---

[big sauce](https://github.com/luiscoco/macOS_Sonoma_VMWare)  
[complementary sauce](https://www.youtube.com/watch?v=77J-hxDmomg)
```
1. first, download vmware 17 <<- google it

2. download macOS 14 iso
https://drive.google.com/file/d/1FdocBt9nSuqGmNWpwkIWqXi7FbuBd8P3/view

3. download unlocker 4
https://github.com/DrDonk/unlocker/releases/download/v4.2.7/unlocker427.zip

4. install vmware 17

5. run unlocker 4 with:
.\unlock.exe
.\check.exe

# ideal .\check.exe result:
Checker 4.2.7 for VMware Workstation/Player
============================================

VMware is installed at:  C:\Program Files (x86)\VMware\VMware Workstation\
VMware version:  20800274
Patch Status: Patched (1)
SHA256: fda33caabab694c11ecd0fe5856f9d1dfd8f3dd443be8c51f8cef8ee5891ce59
Patch Status: Patched (1)
SHA256: a224c5951d4bcb0beffcdd9751084307011433620269387a0af09b1679664e1a
Patch Status: Patched (1)
SHA256: efb2d80800eebadda0448ac254b35f8da5325523cd410f01ccb7bb21dc1f0129
Patch Status: Patched (1)
SHA256: 6c724c837efb33d5d0389576cb344c26f00bbf2cd277f81ff4bbf8ad3b089385

6. now create new VM on vmware 17
Guest operating system: Apple Mac OS X > macOS 14
Max disk size: 100 GB
RAM: 8 GB
Processors: 1 proc, 6 cores
New CD/DVD (SATA): path/to/your/macOS14.iso <<-- IMPORTANT!!
everything else default

7. now startup the VM

8. after choosing Mac OS Language, you will be presented with 4 options: Restore, Install, Safari, Disk Utility

9. CHOOSE DISK UTILITY <<-- IMPORTANT!!

10. Erase VMWare Virtual SATA Hard Drive Media

11. once done, continue with Install Sonoma

12. notice that your NAT isn't working

13. after install, shut down VM

14. open the vmx file in notepad

15. apply the following changes:
smc.version = "0" <<-- APPEND TO BOTTOM
ethernet0.virtualDev = "vmxnet3" <<-- CHANGE EXISTING VALUE

16. startup the VM again, install VMware tools from the VMware top menu
```
optimize macOS vm (optional) [sauce](https://github.com/sickcodes/osx-optimizer)
```
sudo mdutil -i off -a
sudo defaults write /Library/Preferences/com.apple.loginwindow DesktopPicture ""
defaults write com.apple.Accessibility DifferentiateWithoutColor -int 1
defaults write com.apple.Accessibility ReduceMotionEnabled -int 1
defaults write com.apple.universalaccess reduceMotion -int 1
defaults write com.apple.universalaccess reduceTransparency -int 1
```
disable updates (also optional)
```
sudo su
defaults write /Library/Preferences/com.apple.SoftwareUpdate AutomaticDownload -bool false
defaults write com.apple.SoftwareUpdate AutomaticCheckEnabled -bool false
defaults write com.apple.commerce AutoUpdate -bool false
defaults write com.apple.commerce AutoUpdateRestartRequired -bool false
defaults write com.apple.SoftwareUpdate ConfigDataInstall -int 0
defaults write com.apple.SoftwareUpdate CriticalUpdateInstall -int 0
defaults write com.apple.SoftwareUpdate ScheduleFrequency -int 0
defaults write com.apple.SoftwareUpdate AutomaticDownload -int 0
```