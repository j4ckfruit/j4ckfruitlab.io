---
layout: post
title: easily fix ms word sub-headers
tags: pt
---
frustrated when new sub-header isn't following top-level header? [sauce](https://superuser.com/questions/566933/heading-2-is-not-followed-by-heading-1-in-microsoft-word-2010-multilevel-heading)
```
Select a heading, then select Home > Paragraph > Multilevel List > Define new Multilevel List...

Click on More >> at the bottom left of the dialog box.

Make sure everything is set properly in the "Enter formatting for number" textbox by setting the number style and "Include level number from" listbox. Ensure that "Restart list after" is set for the previous list level than thhe one you're editing.
```
