---
layout: post
title: easily setup mobsf on windows
tags: pt mobpt
---

[king guide](https://allabouttesting.org/quick-tutorial-mobsf-installation-on-linux-windows/)

as of 2021-12-03, this is correct (except for if you hit frida ssl installation problem, then call houston, [we got a problem](https://github.com/frida/frida/issues/531))

```
https://www.python.org/ftp/python/3.9.7/python-3.9.7-amd64.exe
https://slproweb.com/download/Win64OpenSSL-3_0_0.exe
https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=Community&rel=17 
https://github.com/git-for-windows/git/releases/download/v2.34.1.windows.1/Git-2.34.1-64-bit.exe
```
```
git clone https://github.com/MobSF/Mobile-Security-Framework-MobSF.git
cd Mobile-Security-Framework-MobSF
.\setup.bat
.\run.bat
```
