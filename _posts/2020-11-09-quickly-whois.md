---
layout: post
title: "quickly whois list of ips"
tags: pt
---

as the title says: quickly generate a list of whois results according to a list of IPs, grabbing just the organization name, and writing results into a file called `ip-owner.txt`:
```
for i in $(cat ips.txt);do whois $i | grep -E "mnt-irt|Organization" | grep -vE "remarks|APNIC|Parent|AMA";done > ip-owner.txt
```
note: this was highly customized to avoid double entries per ip address. you'll have to troubleshoot on your own if you face more than 1 entry per address :) (hint: echo the IP address out)