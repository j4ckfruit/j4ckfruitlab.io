---
layout: post
title: converting vmware to hyper-v
tags: pt
---
first -> [convert local vmdk to vhdx for the hyper-v with starwind-converter](https://theitbros.com/how-to-convert-vmdk-to-vhdx/amp/) -> [(starwind-converter)](https://www.starwindsoftware.com/starwind-v2v-converter)
```
1. install starwind converter (free registration required)
2. select Local file > specify the source VMDK file (.vmdk)
3. select Local file as destination
4. select destination image format as VHD/VHDX (Microsoft Virtual Hard Drive) (VHD to be safe)
```
then -> [create vm in the hyperv and attach the vhdx to the vm](https://www.nucleustechnologies.com/blog/how-to-import-vhd-file-to-hyper-v/)
```
1. To connect VHD to the virtual machine using Hyper-V, you first need to have a version of Windows newer than 2008 and install Hyper-V in your machine. Now follow the below-mentioned steps.
2. Go to the Hyper-V manager center, select the virtual machine and click on Settings.
3. Now, select the controller type that you need to attach to the disk. There are two type of controllers, IDE Controller, and SCSI Controller. Highlight the drive, select any and click on Add.
4. On the next screen, you can change how the selected virtual disk is attached to the virtual machine, need to provide a path to the file, and can also remove the virtual hard disk.
5. From the first Dropdown, select the required controller. The controller is automatically selected but if the required user can select the desired controller.
6. The next will be the location option; there you need to set the location for the controller to attach the disk to. Make sure that you cannot pick a location which is already in use; it will be marked as In Use.
7. Next, you need to perform a search to find the file that you need to attach. For this, click on the Browse button and search for the file.
8. Now, at the last step, you need to apply all the changes that you have made recently. In the settings dialogue box, click on Ok or Apply.
```
