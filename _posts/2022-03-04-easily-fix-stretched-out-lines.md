---
layout: post
title: easily fix stretched-out-lines in ms word
tags: pt
---
[sauce](https://office-watch.com/2020/four-word-fixes-stretched-out-last-line/)
```
1. Go to "Find and Replace"
2. Click "More"
3. Click into "Find what", click "Special" and select "Manual Line Break" -> ^l
4. Click into "Replace with", click "Special" and select "Paragraph Mark" -> ^p
5. Replace All
```
