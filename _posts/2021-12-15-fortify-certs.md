---
title: easily (not really) resolve fortify (java) certificate issues
layout: post
tags: pt
---
so it can get super annoying when your java program goes:
```
Error 6368: No valid SSL certificate available for authentication.
```

but no fret, because thanks to [this hero](https://github-wiki-see.page/m/jamesperrin/developer-training-resources/wiki/Fortify): (on windows):
```
1. visit the SSL-enabled site your java program is trying to reach, e.g. update.fortify.com
2. click on the SSL certificate icon
3. go to Certification Path
4. choose the second last certificate (second from bottom)
5. click View certificate
6. go to Details
7. click Copy to File...
8. save as Base64 encoded cert (.cer) somewhere comfortable
9. now go to your program's jre folder, e.g. \Fortify\Fortify_SCA_and_Apps_20.1.0\jre\bin
10. type this command: (-keystore location might change for you)

.\keytool.exe -import -trustcacerts -keystore ..\lib\security\cacerts -file "C:\path\to\my\cert\mycert.cer"

11. give it any keystore password (that you will remember)
12. try running your java app again - if this doesn't work, try other certificates from step 3
```
update 2022-02-07: if you forgot your keystore password (as i did), do this:
```
1. go to \Fortify\Fortify_SCA_and_Apps_20.1.0\jre\lib\security
2. rename the "cacerts" to "cacerts.old"
3. now re-try the "keystore" command:

.\keytool.exe -import -trustcacerts -keystore ..\lib\security\cacerts -file "C:\path\to\my\cert\mycert.cer"

4. should be now asked to provide a new keystore password
```
