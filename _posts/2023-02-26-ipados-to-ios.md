---
layout: post
title: easily convert iPadOS-only apps to iOS apps
tags: pt
---
[sauce](https://www.reddit.com/r/hearthstone/comments/2258xv/how_to_play_hearthstone_on_iphone_with_the_native/)  

note: assumes you have the IPA (.ipa) file on hand
```
1. rename .ipa file to .zip
2. unzip
3. enter Payload folder
4. edit Info.plist (any text editor will do)
5. search for "UIDeviceFamily"
6. change value to 1
7. zip back the Payload folder
8. rename back to .ipa
```
