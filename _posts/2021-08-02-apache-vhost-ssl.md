---
layout: post
title: easily setup websites with ssl (ubuntu, apache)
tags: pt
---

First make sure you own the domains you want (e.g. example.com) and set A records for both clean and www:
```
Type: A record
Host: @
Value: <ip>
```
```
Type: A record
Host: www
Value: <ip>
```

Setup apache with 2 virtual hosts for both HTTP and HTTPS version of your site (e.g. example.com.conf and example-http.com.conf) [sauce](https://www.rapidsslonline.com/blog/install-ssl-certificate-on-apache2-ubuntu-server/)
```
sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/example.com.conf
sudo cp /etc/apache2/sites-available/example.com.conf /etc/apache2/sites-available/example-http.com.conf
```

Make sure the HTTP one is listening on port 80 and the HTTPS one is on port 443
```
sudo vi /etc/apache2/sites-available/example.com.conf
<VirtualHost *:443>
```
```
sudo vi /etc/apache2/sites-available/example-http.com.conf
<VirtualHost *:80>
```

Enable SSL module on apache [sauce](https://www.digicert.com/kb/csr-ssl-installation/ubuntu-server-with-apache2-openssl.htm)
```
sudo a2enmod ssl
```

Use Let's Encrypt and Certbot to automate SSL Cert creation and Configuration for Apache [sauce](https://www.digitalocean.com/community/tutorials/how-to-secure-apache-with-let-s-encrypt-on-ubuntu-20-04)
```
sudo apt install certbot python3-certbot-apache
```
```
sudo certbot --apache
```
To renew certbot:
```
sudo certbot renew
```
Typical successful certbot output:
```
IMPORTANT NOTES:
 - Congratulations! Your certificate and chain have been saved at:
   /etc/letsencrypt/live/easilogin.com/fullchain.pem
   Your key file has been saved at:
   /etc/letsencrypt/live/easilogin.com/privkey.pem
   Your cert will expire on 2021-10-31. To obtain a new or tweaked
   version of this certificate in the future, simply run certbot again
   with the "certonly" option. To non-interactively renew *all* of
   your certificates, run "certbot renew"
 - If you like Certbot, please consider supporting our work by:

   Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
   Donating to EFF:                    https://eff.org/donate-le
```

Important! Both Port 80 and Port 443 virtual hosts must be up, both clean and www DNS records must be configured for Certbot to work correctly.
