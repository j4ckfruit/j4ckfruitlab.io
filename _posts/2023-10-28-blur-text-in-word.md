---
title: easily blur text in ms word
layout: post
tags: pt
---
```
1. put all your text into a textbox
2. copy and paste *as an image*
3. right-click image, Format Picture
4. go to "Picture Corrections" tab
5. reduce Sharpness
```
