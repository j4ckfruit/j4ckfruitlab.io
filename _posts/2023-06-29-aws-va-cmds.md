---
layout: post
title: aws va tool commands
tags: pt
---
first make sure you're in venv
```
python3 -m venv /opt/venv
source /opt/venv/bin/activate
```

prowler
```
# setup
pip install prowler
```
```
# run
prowler aws -p <profile> -M csv json
```

scoutsuite
```
# setup
# note: scoutsuite supports only up till python3.8 - so will have to use separate python3.8 venv
# note: best to run in ubuntu
sudo apt update
sudo apt install software-properties-common
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt install python3.8
sudo apt install python3.8-venv
python3.8 -m venv /opt/venv-3.8
source /opt/venv-3.8/bin/activate
pip install scoutsuite
scout --help
```
```
# run
scout aws --profile <profile> --region ap-southeast-1
```

cloudsploit
```
# setup
# note: cloudsploit runs on nodejs
sudo apt install nodejs
sudo apt install npm
cd /opt
git clone https://github.com/aquasecurity/cloudsploit.git
cd cloudsploit
npm install
```
```
# run
nodejs ./index.js --csv=cloudsploit.csv --console=none
```
