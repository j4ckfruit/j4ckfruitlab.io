---
layout: post
title: sed cheatsheet
tags: pt
---
with pipe, replace space with nothing
```
cat wow.txt | cut -d":" -f2 | sed -En "s/\s//p"
```
with pipe, remove colour codes
```
sed -r "s/\x1B\[([0-9]{1,3}(;[0-9]{1,2};?)?)?[mGK]//g"
```
