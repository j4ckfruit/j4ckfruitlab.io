---
layout: post
title: easily deploy nodejs app on github pages
tags: nodejs
---
note: assumes you already have a well-configured nodejs app, like [this one](https://github.com/bward2/pacman-js)

```
1. make a repo on github with the nodejs app elements
2. go to "Settings"
3. go to "Pages"
4. under "Build and deployment":
"Source" -> "Deploy from a branch"
"Branch" -> "master", "/ (root)"
5. click "Save"
6. if you see a "Your site is live at https://your-site.github.io/your-nodejs-app/" at the top of the page, do happy dance
```
