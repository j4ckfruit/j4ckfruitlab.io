---
layout: post
title: "cloning sites with wget"
tags: pt
---

```
wget --limit-rate=200k --no-clobber --convert-links --random-wait -r -p -E -e robots=off -U mozilla http://www.nameofthesiteyouwanttocopy.com
```