---
layout: post
title: Useful Impacket SMBServer cmds
tags: smb impacket
---

sharing smb server from linux with user 'test' and password 'test', share name 'tools' and current working directory as shared folder
```
sudo smbserver.py -smb2support -user test -password test tools $(pwd)
```