---
layout: post
title: "quickly hexlify stuff for shellcodes"
tags: pt
---

wanna quickly hexlify your payload and msfvenom doesn't offer that option?
```
cat <payload_file> | xxd -p | tr -d '\n' | sed 's/../\\x&/g'
```

as thankfully explained by two saviours from stackoverflow: [xxd portion](https://superuser.com/questions/279548/how-to-convert-file-data-to-plain-hex) and [sed portion](https://unix.stackexchange.com/questions/402781/add-certain-string-before-every-2-characters)

and the other way: converting a file containing hex characters into a .bin:
```
xxd -r -p input.txt output.bin
```
[saviour](https://stackoverflow.com/questions/7826526/transform-hexadecimal-information-to-binary-using-a-linux-command)
