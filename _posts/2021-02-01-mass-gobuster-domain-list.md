---
layout: post
title: "shell one-liner for mass gobusting"
tags: pt
---
first you need a list of domains called "to_bust.txt" (just the domains)
  

for http:
```
for i in $(cat to_bust.txt);do gobuster dir -u http://$i -w /usr/share/wordlists/dirb/common.txt -o gobuster_$i;done
```
for https (note the 'https' and the '-k'):
```
for i in $(cat to_bust.txt);do gobuster dir -u https://$i -w /usr/share/wordlists/dirb/common.txt -o gobuster_$i -k;done
```
