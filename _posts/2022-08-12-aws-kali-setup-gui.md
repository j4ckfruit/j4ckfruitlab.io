---
layout: post
title: easily setup gui on aws kali
tags: pt
---
install
```
sudo apt-get install -y kali-desktop-xfce xorgxrdp xrdp
```
run
```
sudo systemctl enable xrdp --now
```
check
```
sudo service xrdp status
```
add user
```
sudo useradd -m <UserName>
sudo chsh -s /bin/zsh <UserName>
sudo passwd <UserName>
sudo echo "<UserName>    ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
```
connect: [RDP man](https://docs.microsoft.com/en-us/sysinternals/downloads/rdcman) (or just use native windows RDP client)
