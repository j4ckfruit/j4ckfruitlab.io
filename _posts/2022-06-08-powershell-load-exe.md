---
layout: post
title: powershell encoding exe/dll for reflection
tags: pt
---
[supersauce](https://github.com/PowerShellMafia/PowerSploit/blob/master/ScriptModification/Out-CompressedDll.ps1)
```
$Path = "c:\myexecutable.exe"
$FileBytes = [System.IO.File]::ReadAllBytes($Path)

if (($FileBytes[0..1] | ForEach-Object {[Char]$_}) -join '' -cne 'MZ')
{
    Throw "$Path is not a valid executable."
}

$Length = $FileBytes.Length
$CompressedStream = New-Object IO.MemoryStream
$DeflateStream = New-Object IO.Compression.DeflateStream ($CompressedStream, [IO.Compression.CompressionMode]::Compress)
$DeflateStream.Write($FileBytes, 0, $FileBytes.Length)
$DeflateStream.Dispose()
$CompressedFileBytes = $CompressedStream.ToArray()
$CompressedStream.Dispose()
$EncodedCompressedFile = [Convert]::ToBase64String($CompressedFileBytes)
#write-host $Length

Write-Verbose "Compression ratio: $(($EncodedCompressedFile.Length/$FileBytes.Length).ToString('#%'))"

$Output = @"
`$EncodedCompressedFile = @'
$EncodedCompressedFile
'@
`$DeflatedStream = New-Object IO.Compression.DeflateStream([IO.MemoryStream][Convert]::FromBase64String(`$EncodedCompressedFile),[IO.Compression.CompressionMode]::Decompress)
`$UncompressedFileBytes = New-Object Byte[]($Length)
`$DeflatedStream.Read(`$UncompressedFileBytes, 0, $Length) | Out-Null
[Reflection.Assembly]::Load(`$UncompressedFileBytes)
"@
```
