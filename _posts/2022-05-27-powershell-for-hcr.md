---
layout: post
title: powershell for hcr
tags: pt
---
combining csvs
```
mkdir merged; Get-ChildItem -Filter *.csv | Select-Object -ExpandProperty FullName | Import-Csv | Export-Csv .\merged\merged.csv -NoTypeInformation -Append
```
```
# base cmd - may get stuck in recursive loop - above cmd safer (saves in merged/merged.csv)
Get-ChildItem -Filter *.csv | Select-Object -ExpandProperty FullName | Import-Csv | Export-Csv .\merged.csv -NoTypeInformation -Append
```
