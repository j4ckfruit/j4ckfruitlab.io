---
layout: post
title: powershell b64 encode file (+ sha256)
tags: pt
---
[sauce](https://stackoverflow.com/questions/42592518/encode-decode-exe-into-base64)
```
$file="c:\windows\tasks\cmd.aspx"
get-filehash -algorithm sha256 $file
$bytes = [System.IO.File]::ReadAllBytes($file);
$b64str = [System.Convert]::ToBase64String($bytes);
$b64str
```
